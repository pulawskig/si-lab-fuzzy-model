package si.lab.fuzzy.model;

import com.fuzzylite.Engine;
import com.fuzzylite.activation.General;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.imex.FldExporter;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Gaussian;
import com.fuzzylite.term.Trapezoid;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

public class SiLabFuzzyModel {

    public static void main(String[] args) {
        Engine engine = new Engine();
        engine.setName("SiLabFuzzyModel");
        engine.setDescription("");

        InputVariable service = new InputVariable();
        service.setName("service");
        service.setDescription("");
        service.setEnabled(true);
        service.setRange(0d, 10d);
        service.setLockValueInRange(false);
        service.addTerm(new Gaussian("poor", 0d, 1.5d));
        service.addTerm(new Gaussian("good", 5d, 1.5d));
        service.addTerm(new Gaussian("excellent", 10d, 1.5d));
        engine.addInputVariable(service);

        InputVariable food = new InputVariable();
        food.setName("food");
        food.setDescription("");
        food.setEnabled(true);
        food.setRange(0.000, 10.000);
        food.setLockValueInRange(false);
        food.addTerm(new Trapezoid("rancid", 0.000, 0.000, 1.000, 3.000));
        food.addTerm(new Trapezoid("delicious", 7.000, 9.000, 10.000, 10.000));
        engine.addInputVariable(food);

        OutputVariable tip = new OutputVariable();
        tip.setName("tip");
        tip.setDescription("");
        tip.setEnabled(true);
        tip.setRange(0.000, 30.000);
        tip.setLockValueInRange(false);
        tip.setAggregation(new Maximum());
        tip.setDefuzzifier(new Centroid(200));
        tip.setDefaultValue(Double.NaN);
        tip.setLockPreviousValue(false);
        tip.addTerm(new Triangle("cheap", 0d, 1d/6d, 1d/3d));
        tip.addTerm(new Triangle("average", 1d/3d, 1d/2d, 2d/3d));
        tip.addTerm(new Triangle("generous", 2d/3d, 5d/6d, 1d));
        engine.addOutputVariable(tip);
        
        OutputVariable happiness = new OutputVariable();
        happiness.setName("happiness");
        happiness.setDescription("");
        happiness.setEnabled(true);
        happiness.setRange(0.000, 30.000);
        happiness.setLockValueInRange(false);
        happiness.setAggregation(new Maximum());
        happiness.setDefuzzifier(new Centroid(200));
        happiness.setDefaultValue(Double.NaN);
        happiness.setLockPreviousValue(false);
        happiness.addTerm(new Gaussian("low", 0d, 1.5d));
        happiness.addTerm(new Gaussian("average", 5d, 1.5d));
        happiness.addTerm(new Gaussian("high", 10d, 1.5d));
        engine.addOutputVariable(happiness);

        RuleBlock ruleBlock = new RuleBlock();
        ruleBlock.setName("");
        ruleBlock.setDescription("");
        ruleBlock.setEnabled(true);
        ruleBlock.setConjunction(new Minimum());
        ruleBlock.setDisjunction(new Maximum());
        ruleBlock.setImplication(new Minimum());
        ruleBlock.setActivation(new General());
        ruleBlock.addRule(Rule.parse("if service is poor or food is rancid then tip is cheap and happiness is low", engine));
        ruleBlock.addRule(Rule.parse("if service is good then tip is average and happiness is average", engine));
        ruleBlock.addRule(Rule.parse("if service is excellent or food is delicious then tip is generous and happiness is high", engine));
        engine.addRuleBlock(ruleBlock);
        
        System.out.print(new FldExporter().toString(engine, 9));
        System.out.println();
        
        engine.removeRuleBlock(ruleBlock);
        ruleBlock = new RuleBlock();
        ruleBlock.setName("");
        ruleBlock.setDescription("");
        ruleBlock.setEnabled(true);
        ruleBlock.setConjunction(new Minimum());
        ruleBlock.setDisjunction(new Maximum());
        ruleBlock.setImplication(new Minimum());
        ruleBlock.setActivation(new General());
        ruleBlock.addRule(Rule.parse("if service is very poor or food is rancid then tip is cheap and happiness is not average", engine));
        ruleBlock.addRule(Rule.parse("if service is not good then tip is average and happiness is very low", engine));
        ruleBlock.addRule(Rule.parse("if service is excellent or food is delicious then tip is generous and happiness is high", engine));
        engine.addRuleBlock(ruleBlock);       
        System.out.print(new FldExporter().toString(engine, 9));
    }
}

